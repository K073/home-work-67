import {ADD, CLEAR, DELETE} from "./actions";

const initialState = {
  counter: ''
};


const reducer = (state = initialState, action) => {
  if (action.type === ADD && state.counter.length < 4) {
    return {counter: state.counter + String(action.value)};
  }
  if (action.type === DELETE) {
    return {counter: state.counter.substr(0, state.counter.length - 1)};
  }
  if (action.type === CLEAR) {
    return {counter: state.counter = ''};
  }

  return state;
};



export default reducer;
