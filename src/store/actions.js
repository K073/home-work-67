export const ADD = 'ADD';
export const DELETE = 'DELETE';
export const CLEAR = 'CLEAR';

export const addNumberPin = value => {
  return {type: ADD, value}
};

export const deleteNumberPin = () => {
  return {type: DELETE}
};

export const clearNumberPin = () => {
  return {type: CLEAR}
};