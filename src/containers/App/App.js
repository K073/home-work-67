import React, {Component} from 'react';
import {Button, Col, Grid, Modal, Navbar, Panel, Row} from "react-bootstrap";
import {connect} from "react-redux";
import {addNumberPin, clearNumberPin, deleteNumberPin} from "../../store/actions";

const mapDispatchToProps = dispatch => {
  return {
    addNumberPin: (value) => dispatch(addNumberPin(value)),
    deleteNumberPin: () => dispatch(deleteNumberPin()),
    clearNumberPin: () => dispatch(clearNumberPin())
  };
};

const mapStateToProps = state => {
  return {
    counter: state.counter
  };
};

const CURRENT_PIN = '0791';

class App extends Component {
  state = {
    access: null
  };
  checkAccess = () => {
    if (this.props.counter === CURRENT_PIN)
      this.setState({access: true});
    else
      this.setState({access: false});
  };

  remove = () => {
    this.setState({access: null});
    this.props.deleteNumberPin();
  };

  clear = () => {
    this.setState({access: null});
    this.props.clearNumberPin();
  };

  handleKeyPress = (event) => {
    if (event.key === 'Enter'){
      this.checkAccess()
    }
    if (parseInt(event.key, 10) < 10 && parseInt(event.key, 10) >= 0) {
      this.props.addNumberPin(event.key);
    }
    if (event.key === 'Backspace'){
      this.remove();
    }
    if (event.key === 'Delete'){
      this.clear();
    }
  };

  render() {
    document.body.onkeydown = this.handleKeyPress;
    return (
      <div>
        <Navbar>
          <Navbar.Header>
            <Navbar.Brand>
              Home Work 67
            </Navbar.Brand>
          </Navbar.Header>
        </Navbar>
        <Grid>
          <Row>
            <Col md={3} sm={4} xs={8} xsOffset={2} mdOffset={5} smOffset={4}>
              {this.state.access ?
                <Modal.Dialog>
                  <Modal.Header>
                    <Modal.Title>Access Granted</Modal.Title>
                  </Modal.Header>
                  <Modal.Footer>
                    <Button onClick={() => this.clear()}>Clear</Button>
                  </Modal.Footer>
                </Modal.Dialog> : null}
              <Panel bsStyle={this.state.access === null ? 'default' : this.state.access ? "success" : "danger"}>
                <Panel.Heading>{this.props.counter === '' ? 'password' : new Array(this.props.counter.length).fill('*').join(' ')} </Panel.Heading>
              </Panel>
            </Col>
          </Row>
          <Row>
            <Col md={3} sm={4} xs={8} xsOffset={2} mdOffset={5} smOffset={4}>
              <Row>
                <Col sm={12} className={'text-center'}>
                  <Button onClick={() => this.props.addNumberPin(7)} className={'m5'} bsSize="large">7</Button>
                  <Button onClick={() => this.props.addNumberPin(8)} className={'m5'} bsSize="large">8</Button>
                  <Button onClick={() => this.props.addNumberPin(9)} className={'m5'} bsSize="large">9</Button>
                </Col>
              </Row>
            </Col>
          </Row>
          <Row>
            <Col md={3} sm={4} xs={8} xsOffset={2} mdOffset={5} smOffset={4}>
              <Row>
                <Col sm={12} className={'text-center'}>
                  <Button onClick={() => this.props.addNumberPin(4)} className={'m5'} bsSize="large">4</Button>
                  <Button onClick={() => this.props.addNumberPin(5)} className={'m5'} bsSize="large">5</Button>
                  <Button onClick={() => this.props.addNumberPin(6)} className={'m5'} bsSize="large">6</Button>
                </Col>
              </Row>
            </Col>
          </Row>
          <Row>
            <Col md={3} sm={4} xs={8} xsOffset={2} mdOffset={5} smOffset={4}>
              <Row>
                <Col sm={12} className={'text-center'}>
                  <Button onClick={() => this.props.addNumberPin(1)} className={'m5'} bsSize="large">1</Button>
                  <Button onClick={() => this.props.addNumberPin(2)} className={'m5'} bsSize="large">2</Button>
                  <Button onClick={() => this.props.addNumberPin(3)} className={'m5'} bsSize="large">3</Button>
                </Col>
              </Row>
            </Col>
          </Row>
          <Row>
            <Col md={3} sm={4} xs={8} xsOffset={2} mdOffset={5} smOffset={4}>
              <Row>
                <Col sm={12} className={'text-center'}>
                  <Button onClick={() => this.remove()} className={'m5'} bsStyle="danger" bsSize="large">{'<'}</Button>
                  <Button onClick={() => this.props.addNumberPin(0)} className={'m5'} bsSize="large">0</Button>
                  <Button onClick={() => this.checkAccess()} className={'m5'} bsStyle="success" bsSize="large">E</Button>
                </Col>
              </Row>
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
